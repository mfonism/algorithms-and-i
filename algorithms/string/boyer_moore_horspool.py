from typing import Dict


class BadCharacterTable:
    def __init__(self, pattern: str):
        assert len(pattern)

        iter_val = reversed(range(len(pattern)))
        self.table: Dict[str, int] = {
            char: val for (char, val) in zip(pattern, iter_val)
        }
        self.table[pattern[-1]] = len(pattern)
        self.string = pattern

    def __getitem__(self, key: str) -> int:
        return self.table.get(key, len(self.string))


class BMHEngine:
    def __init__(self, pattern: str):
        self.pattern = pattern
        self.bad_character_table = BadCharacterTable(pattern)

    def find_in(self, text: str):
        i = 0
        pattern_length = len(self.pattern)

        # while there's still space from the ith position in the text till
        # the -1th position in it to fit in the pattern
        #     perform the right to left comparison of characters in pattern
        #     to characters in the text as follows:
        #
        #     if there is a mismatch at some position
        #         shift the pattern according to the bad character heuristic
        #         restart the right to left comparison
        #     otherwise if we've walked back to the 0th character in the pattern
        #         then all the characters have matched up until this one
        #         and this position in the text is where the match starts
        while (i + pattern_length) <= len(text):
            for j in reversed(range(pattern_length)):
                if text[i + j] != self.pattern[j]:
                    num_matches = pattern_length - 1 - j
                    i += max(self.bad_character_table[text[i + j]] - num_matches, 1)
                    break
                if j == 0:
                    return i

        return -1
