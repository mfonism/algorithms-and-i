# Boyer-Moore-Horspool

[string/boyer_moore_horspool.py](string/boyer_moore_horspool.py)

The best, most complete demonstration of this algorithm **which I found** online is https://www.youtube.com/watch?v=4Oj_ESzSNCk

# Subset Sum Problem (Draft version)

[array/subset_sum_problem.py](array/subset_sum_problem.py)

I watched the following videos where the tables were created by hand:

- https://www.youtube.com/watch?v=dJmyfFC3-3A
- https://www.youtube.com/watch?v=s6FhG--P7z0

They helped me figure out the algorithm in a super intuitive way. I was so excited to capture an intuitive understanding of the creation of the auxiliary table &mdash; I know why we place whatever number we place into each cell.

Understanding the reason for the _backtracking_ step has inspired me to look again at the Knutt-Morris-Pratt string search algorithm. I had looked at it some days ago, but could not understand the _backtracking_ step.
