# This is just a DRAFT!
# I'll polish it later

"""
Subset sum problem

Given:
* A: array of n non-negative integers
* target: integer

Find:
Whether a subset exists in A whose sum is target
"""

A = ...
target = ...
# create a matrix Mij
# whose rows, i, are indexed by the indices of the array A
# and whose columns, j, are indexed by the numbers from 0 to target
M = [[0] * target for _ in range(len(A) + 1)]


for i in range(len(A)):
    # the 0th column has 1 in it
    M[i][0] = 1

    # figure out what to insert into the rest of the columns
    for j in range(1, target):
        # see j as current target

        # if the ith element in A is equal to current target
        # then this element can make up a singleton set that
        # sums up to current target
        # insert 1 and continue iteration
        if A[i] == j:
            M[i][j] = 1
            continue

        # if the ith element in A is greater than current target
        # then it plays no role in creating a subset that sums to target
        # so, it just propagates whether such a subset could be created
        # as at the previous element in A -- the (i - 1)th element
        # ----
        # the realisation is that if such a subset could be created
        # in the set before I joined in, then it can still be created
        # after I joined in
        if A[i] > j:
            # if there's a previous element in A
            # propagate whether the target could be reached as at then
            if i > 0:
                M[i][j] = M[i - 1][j]
            else:
                # no previous element
                # target could not possibly be reached
                M[i][j] = 0  # this is set by default, though
            continue

        # So, A[i] < j
        # A[i] can contribute towards j if j - A[i] could be formed as at A[i-1]
        # ----
        # in other words...
        # if a subset could be formed as at the (i - 1)th element in A
        # whose sum is shy of the current target just by the current element in A
        # then a subset which sums up to the current target
        # can be created by adding the current element to it
        if M[i - 1][j - A[i]] == 1:
            M[i][j] = 1

# M[i][j] essentially tells us whether we can create a subset
# of elements in A from A[0] to A[i] inclusive
# which sums up to j
# ----
# so our result is M[len(A) - 1][target]
M[len(A) - 1][target]
