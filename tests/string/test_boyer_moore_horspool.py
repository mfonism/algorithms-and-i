from string import ascii_lowercase

import pytest

from algorithms.string.boyer_moore_horspool import BadCharacterTable, BMHEngine


@pytest.mark.parametrize(
    "pattern, mapping",
    [
        ("abcd", {"a": 3, "b": 2, "c": 1, "d": 4}),
        ("tooth", {"t": 1, "o": 2, "h": 5}),
        ("surana", {"s": 5, "u": 4, "r": 3, "n": 1, "a": 6}),
    ],
)
def test_bad_character_table_creation(pattern, mapping):
    table = BadCharacterTable(pattern)

    for k, v in mapping.items():
        assert table[k] == v

    alien_chars = set(ascii_lowercase) - set(pattern)
    assert all(table[c] == len(pattern) for c in alien_chars)


def test_find():
    engine = BMHEngine("horse")
    assert engine.find_in("horses are rabbits on steroid") == 0
    assert engine.find_in("i'm gonna take my horse to the old town road") == 18
    assert engine.find_in("in my house i have a hose") == -1
    assert engine.find_in("in my house i have a host of horses") == 29

    engine = BMHEngine("tahah")
    assert engine.find_in("abtahah") == 2

    engine = BMHEngine("thuth")
    assert engine.find_in("we hold thuth") == 8
